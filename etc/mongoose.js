const config = require('../config/config.json');
const mongoose = require('mongoose');
mongoose.connect(config.mongoose.uri, config.mongoose.options);
mongoose.Promise = global.Promise;

mongoose.connection.on('connected', function() {
    console.info('connected');
});

/* die on error
 mongoose.connection.on('error', function(err)  {
 console.error("Mongoose error", err);
 });
 */

console.info("DB initialized");

module.exports = mongoose;