const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const async = require('async');
const _ = require('lodash');
const User = require('../models/user').User;

const authenticationMiddleware = require('../middleware/auth');


module.exports = function () {
    passport.use(new LocalStrategy(
        function(username, password, done) {
            async.waterfall([
                function (callback) {
                    User.findOne({username: username}, callback);
                },
                function (user, callback) {
                    console.log(user);
                    if (user) {
                        if (user.checkPassword(password)) {
                            callback(null, user);
                        } else {
                            callback(null, user, ['Неверный пароль']);
                        }
                    }
                }
            ], function (err, user, result) {
                if (err) return done(err);
                req.session.user = user._id;
                if (_.isArray(result)) {
                    return done(null, false, { message: _.toString(result).replace(',', ', ') })
                } else {
                    return done(null, user)
                }
            });
        }
    ));
    passport.authenticationMiddleware = authenticationMiddleware
};
