const Log = require('../models/log').Log;


module.exports = function (userid, message) {
    const log = new Log({userid, message});
    log.save();
}