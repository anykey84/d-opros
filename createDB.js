const mongoose = require('./etc/mongoose');
const async = require('async');

async.series([
    open,
    dropDatabase,
    requireModels,
    createUsers,
    createUserStatuses,
    createQuestions
], function (err) {
    console.log(arguments);
    mongoose.disconnect();
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    const db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('./models/user').User;
    require('./models/userStatus').UserStatus;
    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback)
}

function createUsers(callback) {

    const users = [
        {
            _id : '59802842f47c382deca5cba7',
            username: 'ouadmin',
            password: 'CbkrH0flCbkrH0fl',
            email: 'foxycoder@yandex.ru',
            confirmed_email: 'foxycoder@yandex.ru',
            phone: '79996006650',
            status: 100,
            confirmed_phone: '79996006650'
        },
        {
            _id : '59802ac131e8f4325048d088',
            username: 'h_raushan',
            password: '179833',
            email: 'h_raushan@mail.ru',
            confirmed_email: 'h_raushan@mail.ru',
            phone: '79608517979',
            confirmed_phone: '79608517979',
            status: 1
        },
        {
            _id : '59802f0167e1a8277cc282b2',
            username: 'milaev',
            password: 'milaev@bk.ru',
            email: 'milaev@bk.ru',
            confirmed_email: 'milaev@bk.ru',
            phone: '79608622202',
            confirmed_phone: '79608622202',
            status: 1
        }
    ];
    async.each(users, function (userData, callback) {
        const user = new mongoose.models.User(userData);
        user.save(callback);
    }, callback);
}

function createUserStatuses(callback) {

    const userStatuses = [
        { statusValue: 0, statusName: 'Предварительная регистрация' },
        { statusValue: 1, statusName: 'Участник' },
        { statusValue: 100, statusName: 'Администратор' }
    ];
    async.each(userStatuses, function (userStatusData, callback) {
        const userStatus = new mongoose.models.UserStatus(userStatusData);
        userStatus.save(callback);
    }, callback);
}

function createQuestions(callback) {
    require('./models/question').Question;

    const questions = [
        {
            type: 'radio',
            yourOption : false,
            text : "Question 1",
            answers : [
                {
                    1 : "Question 1 Answer 1",
                    2 : "Question 1 Answer 2",
                    3 : "Question 1 Answer 3",
                    4 : "Question 1 Answer 4",
                }
            ]
        },
        {
            type: 'radio',
            yourOption : false,
            text : "Question 2",
            answers : [
                {
                    1 : "Question 2 Answer 1",
                    2 : "Question 2 Answer 2",
                    3 : "Question 2 Answer 3",
                    4 : "Question 2 Answer 4",
                }
            ]
        },
        {
            type: 'radio',
            yourOption : false,
            text : "Question 3",
            answers : [
                {
                    1 : "Question 3 Answer 1",
                    2 : "Question 3 Answer 2",
                    3 : "Question 3 Answer 3",
                    4 : "Question 3 Answer 4",
                }
            ]
        },
        {
            type: 'radio',
            yourOption : false,
            text : "Question 4",
            answers : [
                {
                    1 : "Question 4 Answer 1",
                    2 : "Question 4 Answer 2",
                    3 : "Question 4 Answer 3",
                    4 : "Question 4 Answer 4",
                }
            ]
        }
    ];
    async.each(questions, function (questionData, callback) {
        const question = new mongoose.models.Question(questionData);
        question.save(callback);
    }, callback);
}
