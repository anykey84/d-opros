$(function(){


	$("#portfolio-contant-active").mixItUp();


	$("#testimonial-slider").owlCarousel({
	    paginationSpeed : 500,      
	    singleItem:true,
	    autoPlay: 3000,
	});

    $(".partner-url input").focus(function(){
        if(this.value == this.defaultValue){
            this.select();
        }
    });


	$("#clients-logo").owlCarousel({
		autoPlay: 3000,
		items : 5,
		itemsDesktop : [1199,5],
		itemsDesktopSmall : [979,5],
	});

	$("#works-logo").owlCarousel({
		autoPlay: 3000,
		items : 5,
		itemsDesktop : [1199,5],
		itemsDesktopSmall : [979,5],
	});


	// google map
		var map;
		function initMap() {
		  map = new google.maps.Map(document.getElementById('map'), {
		    center: {lat: -34.397, lng: 150.644},
		    zoom: 8
		  });
		}


	// Counter

	$('.counter').counterUp({
        delay: 10,
        time: 1000
    });


    // $('form#register').on('submit', function (event) {
    //     event.preventDefault();
    //     console.log('register');
    //     var form = $(this);
    //
    //     $(':submit', form).button("loading");
    //
    //     $.ajax({
    //         url: "/register",
    //         method: "POST",
    //         data: form.serialize(),
    //         complete: function () {
    //             $(":submit", form).button("reset");
    //         },
    //         success: function (result) {
    //             console.log(result);
    //             if (result.error){
    //                 swal('Ошибка регистрации', result.error, 'error')
    //             } else {
    //                 swal({
    //                     title: "Успешная регистрация",
    //                     text: "",
    //                     timer: 5000,
    //                     showConfirmButton: false
    //                 });
    //                 setTimeout(function(){
    //                     window.location.href = '/login';
    //                 }, 1000);
    //             }
    //         }
    //
    //     })
    //
    // });

    // $('form#login').on('submit', function (event) {
    //     event.preventDefault();
    //     console.log('login');
    //     var form = $(this);
    //
    //     $(':submit', form).button("loading");
    //
    //     $.ajax({
    //         url: "/login",
    //         method: "POST",
    //         data: form.serialize(),
    //         complete: function () {
    //             $(":submit", form).button("reset");
    //         },
    //         success: function (result) {
    //             console.log(result);
    //             if (result.error){
    //                 swal('Произошла ошибка. Попробуйте еще раз', result.error, 'error')
    //             } else {
    //                 swal({
    //                     title: "Успешная авторизация",
    //                     text: "Сейчас Вы будете перемещены в личный кабинет",
    //                     timer: 3000,
    //                     showConfirmButton: false
    //                 });
    //                 setTimeout(function(){
    //                     window.location.href = '/lk';
    //                 }, 3000);
    //             }
    //         }
    //
    //     })
    //
    // })

    $('input[type=tel]').mask(" +7 (999) 999-9999")

});




