const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    userId: {
        type: String,
        required: true
    },
    questionId: {
        type: String,
        required: true
    },
    answers: {
        type : Array,
        required : true
    },
    timestamp : {
        type: Date,
        default: Date.now
    }
});


exports.Answer = mongoose.model('Answer', schema);