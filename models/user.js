const crypto = require('crypto');
const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    username: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    email: {
        type: String
    },
    partner_id: {
        type: String
    },
    active: {
        type: Boolean,
        default: true
    },
    confirmed_email: {
        type: String
    },
    confirmed_phone: {
        type: String
    },
    status: {
        type: Number,
        default: 0
    },
    poolCount: {
        type: Number,
        default: 0
    },
    hashedPassword: {
        type: String
    },
    salt: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
    resetPasswordToken: {
        type: String
    },
    resetPasswordExpires: {
        type: String
    },
    confirmEmailToken: {
        type: String
    },
    confirmEmailTokenExpires: {
        type: String
    },
    partnerId: {
        type: String
    },
    confirmPhoneToken: {
        type: String
    },
    confirmPhoneTokenExpires: {
        type: String
    },
    confirmAccountToken: {
        type: String
    }
});

schema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function(password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() { return this._plainPassword; });


schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.methods.getPublicFields = function() {
    return {
        username: this.username,
        created: this.created,
        id: this.id
    };
};

exports.User = mongoose.model('User', schema);