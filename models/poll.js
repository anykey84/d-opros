const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    title: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
    questions: {
        type : Array,
        required : true
    }
});


exports.Poll = mongoose.model('Poll', schema);