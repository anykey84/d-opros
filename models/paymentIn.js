const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    userId: {
        type: String,
        required: true
    },
    orderId: {
        type: String,
        required: true
    },
    summ: {
        type: Number,
        required: true
    },
    datePay: {
        type: Date
    }
});


exports.PaymentIn = mongoose.model('PaymentIn', schema);