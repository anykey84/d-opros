const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    userId: {
        type: String,
        required: true
    },
    summ: {
        type: Number,
        required: true
    },
    paymentType: {
        type: Number,
        required: true
    },
    isPaid: {
        type: Boolean,
        default: false
    },
    datePay: {
        type: Date
    },
    paymentComment: {
        type: String
    },
    number: {
        type: String,
        required: true
    }
});


exports.Order = mongoose.model('Order', schema);