const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    type: {
        type: String,
        required: true
    },
    yourOption: {
        type: Boolean,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    answers: {
        type : Array,
        required : true
    },
    order : {
        type : Number,
        default : 0
    },
    active : {
        type: Boolean,
        default: true
    }
});


exports.Question = mongoose.model('Question', schema);