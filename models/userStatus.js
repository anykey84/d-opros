const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    statusValue: {
        type: Number,
        required: true
    },
    statusName: {
        type: String,
        required: true
    }
});


exports.UserStatus = mongoose.model('UserStatus', schema);