const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    userid: {
        type: String,
        required: true
    },
    message: {
        type : String,
        required : true
    }
});


exports.Log = mongoose.model('Log', schema);