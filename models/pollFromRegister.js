const mongoose = require('../etc/mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    fromUser: {
        type: String,
        required: true
    },
    toUser: {
        type: String,
        required: true
    },
    isCompleted: {
        type : Boolean,
        default : false
    },
    summ: {
        type : Number,
        required : true
    }
});


exports.PollfromRegister = mongoose.model('PollfromRegister', schema);