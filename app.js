const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const flash = require('express-flash');

const index = require('./routes/index');
const admin = require('./routes/admin');
const register = require('./routes/register');
const login = require('./routes/login');
const lk = require('./routes/lk');
const logout = require('./routes/logout');
const forgot = require('./routes/forgot');
const reset = require('./routes/reset');
const emailConfirm = require('./routes/email-confirm');
const phoneConfirm = require('./routes/phone-confirm');
const accountConfirm = require('./routes/account-confirm');
const poll = require('./routes/poll');
const agreement = require('./routes/agreement');
const pay = require('./routes/pay');

const authenticationMiddleware = require('./middleware/auth');
const isAdminMiddleware = require('./middleware/isAdmin');

const mongoose = require('./etc/mongoose');


const app = express();
const config = require('./config/config.json');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

require('./etc/authInit');

const MongoStore = require('connect-mongo')(session);

app.use(session({
    secret: config.session.secret,
    key: config.session.key,
    cookie: config.session.cookie,
    store: new MongoStore({mongooseConnection : mongoose.connection}),
    resave: true,
    saveUninitialized: true
}));

app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use(require('./middleware/loadUser'));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/register', register);
app.use('/login', login);
app.use('/lk', authenticationMiddleware, lk);
app.use('/ouadmin', authenticationMiddleware, isAdminMiddleware, admin);
app.use('/logout', logout);
app.use('/forgot', forgot);
app.use('/reset', reset);
app.use('/agreement', agreement);
app.use('/pay', pay);
app.use('/confirm/email', emailConfirm);
app.use('/confirm/phone', phoneConfirm);
app.use('/confirm/account', accountConfirm);
app.use('/poll', authenticationMiddleware, poll);


app.get('/.well-known/acme-challenge/9oaU4xH50w2cHjnrBHGR-ONmD7FMLNJd3NAzXQMpscA', function(req,res){
    res.send('9oaU4xH50w2cHjnrBHGR-ONmD7FMLNJd3NAzXQMpscA.imnrkuavz35gUklHI3NijkMl3-U5cC8yDGYuG44X-6E');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : err;

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
