const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const Order = require('../models/order').Order;
const async = require('async');
const _ = require('lodash');
const ObjectId = require('mongodb').ObjectId;
const prices = require('../config/config.json').prices;
const moment = require('moment');
const mailerTransporter = require('../etc/mailerTransporter');
const config = require('../config/config.json');

/* GET home page. */
// router.get('/', function (req, res, next) {
//     res.render('register', {title: 'Регистрация', partner_id: ''});
// });

router.get('/:id', function (req, res, next) {
    try{
        let id = new ObjectId(req.params.id);
    } catch (e){
        return next();
    }
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        if (!user){
            return next();
        }
        res.render('register', {title: 'Регистрация', partner_id: req.params.id});
    });
});

router.post('/', function (req, res, next) {
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;
    const repassword = req.body.repassword;
    const phone = req.body.phone;
    const partner_id = req.body.partner_id;
    const agree = req.body.agree;

    let errorArr = [];

    if (password !== repassword) {errorArr.push("Пароли не совпадают");}
    if (password.length < 5) {errorArr.push("длина пароля должна быть не менее 5 символов");}
    if (!agree) {errorArr.push("Вы должны согласиться с условиями пользовательского соглашения");}

    if (errorArr.length){
        req.flash('error',  _.toString(errorArr).replace(',', ', '));
        res.render('register', {title: 'Регистрация', partner_id: partner_id});
    } else {
        async.waterfall([
            function (callback) {
                User.findOne({confirmed_email: email}, callback);
            },
            function (user, callback) {
                if(!user){
                    User.findOne({
                        $and : [
                            {email: email},
                            {status: {$gt : 0}}
                        ]
                    }, callback);
                } else {
                    return callback(null, user);
                }
            },
            function (user, callback) {
                User.findById(partner_id, function (err, partner) {
                    if (err) return callback(err);
                    if (!partner){
                        return callback(err, errorArr);
                    }
                    return callback(null, errorArr, user, partner);
                });

            },
            function (errorArr, user, partner, callback) {
                if (!user) {
                    const newuser = new User({
                        username: username,
                        password: password,
                        email: email,
                        partner_id: partner_id,
                        phone: phone.replace(/[^0-9]/gim,''),
                        partnerId: partner._id
                    });
                    //let poolCount = partner.poolCount + 1;
                    //User.findByIdAndUpdate(partner._id, {$set: {poolCount: poolCount}});
                    newuser.save(function (err, nUser) {
                        if (err) return next(err);
                        callback(null, errorArr, nUser);
                    })

                } else {
                    errorArr.push('Данный email уже зарегистрирован. Если он принадлежит Вам, попробуйте восстановить пароль');
                    callback(null, errorArr, user)
                }
            },
            function (errorArr, user, callback) {
                if (user && !errorArr.length) {
                    let order = new Order({
                        userId: user.id,
                        summ: prices.register,
                        paymentType: 1,
                        paymentComment: "Регистрация на сайте d-opros.com",
                        number: `${Math.floor(Math.random() * 999999) + 1}${moment().format("SSS")}`
                    });
                    order.save(function (err, newOrder) {
                        if (err) return next(err);
                        callback(null, errorArr, newOrder, user);
                    });
                } else {
                    callback(null, errorArr)
                }
            }
        ], function (err, errorArr, order, user) {
            if (err) return next(err);
            if (errorArr.length){
                req.flash('error',  _.toString(errorArr).replace(',', ', '));
                //res.redirect(`/register/${partner_id}`);
                res.render('register', {title: 'Регистрация', partner_id: partner_id});
            } else {
                const mailOptions = {
                    to: config.adminEmail ,
                    from: config.serverEmail,
                    subject: '[d-opros] new registration',
                    html: `<p>Новая регистрация</p>
                            <p>Пользователь:</p>
                            <p>${user.id}</p>
                            <p>${user.name}</p>
                            <p>${user.email}</p> `
                };
                mailerTransporter.sendMail(mailOptions);
                res.redirect(`/pay/${order.id}`);
            }
        });
    }
});

module.exports = router;
