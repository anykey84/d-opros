const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const Order = require('../models/order').Order;
const ObjectId = require('mongodb').ObjectId;
const ToLog = require('../etc/writeToLog');

router.get('/', function(req, res, next) {
    res.render('admin/admin', { title: 'Админпанель'});
});

router.get('/users', function(req, res, next) {
    User.find({}, function (err, users) {
        if (err) return next(err);
        res.render('admin/users', {users, title: 'Админпанель - Пользователи'})
    });
});

router.get('/orders', function(req, res, next) {
    Order.find({}, function (err, orders) {
        if (err) return next(err);
        //res.json(users);
        res.render('admin/orders', {orders, title: 'Админпанель - Счета'})
    });
});

router.get('/users/:id', function(req, res, next) {
    try{
        let id = new ObjectId(req.params.id);
    } catch (e){
        return next();
    }
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        if (!user){
            return next();
        }
        //res.json(user);
        res.render('admin/user', {user, title: 'Админпанель - Пользователь'})
    });
});

router.get('/users/delete/:id', function(req, res, next) {
    try{
        let id = new ObjectId(req.params.id);
    } catch (e){
        return next();
    }
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        if (!user){
            return next();
        }
        //res.json(user);
        res.render('admin/user-delete', {user, title: 'Админпанель - Удаление пользователя'})
    });
});


router.get('/users/remove/:id', function (req, res, next) {
    let id = new ObjectId(req.params.id);

    User.findByIdAndRemove(id, function(err) {
        if (err) return next(err);

        req.flash("success", "Пользователь удален");
        res.redirect("/ouadmin/users");
    });
});


router.get('/users/edit/:id', function(req, res, next) {
    let currentUser = req.user;
    let id = new ObjectId(req.params.id);
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        if (!user){
            return next();
        }
        //res.json(user);
        res.render('admin/user-edit', {user, title: 'Админпанель - Редактирование пользователя', currentUser})
    });
});

router.get('/users/change-status/:id/:status', function (req, res, next) {
    let id = new ObjectId(req.params.id);
    let status = req.params.status;
    let currentUser = req.user;
    if (currentUser.id === req.params.id){
        req.flash("error", "Нельзя изменить свой статус");
        res.redirect("/ouadmin/users/edit/"+req.params.id);
    } else {
        User.findById(id, function(err, user) {
            if (err) return next(err);

            user.status = status;

            user.save(function (err) {
                if (err) return next(err);

                req.flash("success", "Статус изменен");
                res.redirect("/ouadmin/users/edit/"+req.params.id);
            });
        });
    }
});

module.exports = router;
