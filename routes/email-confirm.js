const express = require('express');
const router = express.Router();
const async = require('async');
const mailerTransporter = require('../etc/mailerTransporter');
const crypto = require('crypto');
const User = require('../models/user').User;
const config = require('../config/config.json');


router.get('/', function(req, res) {
    res.render('lk', {
        user: req.user
    });
});

router.get('/:token', function(req, res) {
    User.findOne({ confirmEmailToken: req.params.token, confirmEmailTokenExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
            req.flash('error', 'Email confirm token is invalid or has expired.');
            return res.redirect('/lk');
        }

        user.confirmed_email = user.email;
        user.confirmEmailToken = undefined;
        user.confirmEmailTokenExpires = undefined;

        user.save(function() {
            req.flash('success', 'Email успешно подтвержден');
            return res.redirect('/lk');
        });
    });
});

router.post('/', function(req, res, next) {

    async.waterfall([
        function(done) {
            crypto.randomBytes(20, function(err, buf) {
                const token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (!user) {
                    req.flash('error', 'Данный email не принадлежит ни одной учетной записи');
                    return res.redirect('/lk');
                }
                if (user.confirmed_email === req.body.email){
                    req.flash('error', 'Данный email уже подтвержден');
                    return res.redirect('/lk');
                }
                user.confirmEmailToken = token;
                user.confirmEmailTokenExpires = Date.now() + 3600000; // 1 hour

                user.save(function(err) {
                    done(err, token, user);
                });
            });
        },
        function(token, user, done) {
            const mailOptions = {
                to: user.email,
                from: config.serverEmail,
                subject: 'д-опрос: подтверждение e-mail',
                html: '<p>Вы получили это письмо потому, что Вы (или кто-то другой) запросили подтверждение email на сайте d-opros.com.</p>' +
                '<p>Для продолжения кликните по ссылке или скопируйте ее в адресную строку браузера:</p>' +
                '<a href="http://' + req.headers.host + '/confirm/email/' + token + '">' +
                'http://' + req.headers.host + '/confirm/email/' + token  +
                '</a>'  +
                '<p>Если Вы не запрашивали подтверждение email, игнорируйте это письмо</p>'
            };
            mailerTransporter.sendMail(mailOptions, function(err) {
                req.flash('info', 'Письмо с дальнейшими инструкциями было отправлено по адресу ' + user.email);
                done(err, 'done');
            });
        }
    ], function(err) {
        if (err) req.flash('error', 'ошибка отправки e-mail');
        res.redirect('/lk');
    });
});

module.exports = router;
