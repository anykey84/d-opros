const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const async = require('async');


router.get('/:token', function(req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('/forgot');
        }
        res.render('reset', {
            user: req.user
        });
    });
});

router.post('/:token', function(req, res) {
    async.waterfall([
        function(done) {
            User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
                if (!user) {
                    req.flash('error', 'Password reset token is invalid or has expired.');
                    return res.redirect('back');
                }

                user.password = req.body.password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save(function(err) {
                    req.logIn(user, function(err) {
                        done(err, user);
                    });
                });
            });
        },
        function(user, done) {
            let options = {
                service: 'Yandex',
                auth: {
                    user: 'foxycoder@yandex.ru',
                    pass: 'xyfogoZw0ruzeW'
                }
            };
            let transporter = nodemailer.createTransport(options);
            let mailOptions = {
                to: user.email,
                from: 'foxycoder@yandex.ru',
                subject: 'Your password has been changed',
                html: '<p>Привет,</p>' +
                '<p>Пароль для email ' + user.email + ' был успешно изменен.</p>'
            };
            transporter.sendMail(mailOptions, function(err) {
                req.flash('success', 'Пароль успешно изменен');
                done(err);
            });
        }
    ], function(err) {
        res.redirect('/login');
    });
});

module.exports = router;
