const express = require('express');
const router = express.Router();
const async = require('async');
const User = require('../models/user').User;
const PollFromRegister = require('../models/pollFromRegister').PollfromRegister;
const Answer = require('../models/answer').Answer;
const Question = require('../models/question').Question;
const _ = require('lodash');

/* GET home page. */
router.get('/', function (req, res, next) {
    let user = req.user;

    PollFromRegister.find({toUser : user.id}, function (err, polls) {
        if (err) next(err);

        let completed = polls.filter(function (poll) {
            return poll.isCompleted === true;
        });
        res.render('poll', {
            polls : {
                total : polls,
                completed,
                incompleted: polls.length - completed.length
            },
        });
    });
});

router.get('/next', function (req, res, next) {
    let user = req.user;
    console.log(user.id);

    PollFromRegister.count({toUser : user.id, isCompleted : false}, function (err, pollsCount) {
        if (err) next(err);
        if (!pollsCount) {
            req.flash('success', 'Все опросы пройдены');
            res.redirect('/lk')
        } else {
            async.waterfall([
                function (callback) {
                    Answer.find({userId : user.id}, 'questionId', callback)
                },
                function (answers, callback) {
                    let answersIds = _.map(answers, 'questionId');
                    console.log(answersIds);
                    Question.find({active : true, _id: { $nin: answersIds }}, 'order', function (err, questions) {
                        callback(null, questions)
                    })
                },
                function (questions, callback) {
                    let question = _.minBy(questions, 'order');
                    Question.findById(question._id, callback)
                }
            ], function (err, question) {
                if (err) next (err);
                console.log(question);
                res.render('question', {question})
            });

        }
    });
});

router.post('/submit', function (req, res, next) {
    let user = req.user;
    let questionId = req.body.questionId;
    let option = req.body.option;
    if (!user._id) {
        req.flash('error', 'Срок жизни сессии истек');
        res.redirect('/lk')
    }
    if (!option) {
        req.flash('error', 'Выберите вариант ответа');
        res.redirect('/poll/next')
    }
    option = option.split('|');
    let answer = new Answer({
        userId: user._id,
        questionId,
        answers: [
            {
                number: option[0],
                text: option[1]
            }
        ]
    });
    answer.save(function (err) {
        if (err) next(err);
        PollFromRegister.findOneAndUpdate({toUser : user._id, isCompleted : false}, {isCompleted : true}, function (err, poll) {
            if (err) next (err);
            console.log(poll);
            req.flash('success', 'Ответ принят. Вознаграждение начислено');
            res.redirect('/poll');
        })
    })
})

module.exports = router;
