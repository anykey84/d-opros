const express = require('express');
const router = express.Router();
const async = require('async');
const mailerTransporter = require('../etc/mailerTransporter');
const Order = require('../models/order').Order;
const crypto = require('crypto');


router.get('/', function(req, res) {

    if(req.user){
        async.waterfall([
            function(callback) {
                Order.findOne({userId : req.user._id}, function(err, order) {
                    if (err) callback(err);
                    if (!order) {
                        req.flash('error', 'Счет не существует');
                        return res.redirect('/lk');
                    }
                    callback(null, order.id)
                });
            }
        ], function(err, orderId) {
            if (err) req.flash('error', 'ошибка активации');
            console.log(orderId);
            res.render('account-confirm', {orderId});
        });

    } else {
        res.redirect('/lk');
    }



});

router.get('/:token', function(req, res) {


    async.waterfall([
        function(done) {
            User.findOne({confirmAccountToken: req.params.token} , function(err, user) {
                if (!user) {
                    req.flash('error', 'Account confirm token is invalid');
                    return res.redirect('/confirm/account');
                }

                user.confirmAccountToken = undefined;
                user.status = 1;

                user.save(function(err) {
                    req.flash('success', 'Учетная запись успешно подтверждена');
                    done(err, user)
                });
            });
        },
        function(user, done) {
            User.findById(user.partnerId, function(err, user) {
                if (!user) {
                    req.flash('error', 'Invalid partner token');
                    return res.redirect('/confirm/account');
                }

                user.poolCount = user.poolCount + 1;

                user.save(function(err) {
                    done(err, 'done');
                });
            });
        }
    ], function(err) {
        if (err) req.flash('error', 'ошибка активации учетной записи');
        res.redirect('/confirm/account');
    });

});



module.exports = router;
