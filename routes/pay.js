const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const prices = require('../config/config.json').prices;
const Order = require('../models/order').Order;
const User = require('../models/user').User;
const PollfromRegister = require('../models/pollFromRegister').PollfromRegister;
const config = require('../config/config.json');
const async = require('async');

router.get('/', function(req, res, next) {

    let mrh_login = 'd-opros.com';
    let mrh_pass1 = 't2vRWUADqQBc956RhI8G';
    let inv_id = Math.floor(Math.random() * 1000) + 1;
    let inv_desc = "Access to D-opros.com";
    let out_sum = prices.register;
    let toHashString = `${mrh_login}:${out_sum}:${inv_id}:${mrh_pass1}`;
    let md5Hash = crypto.createHash('md5')
        .update(toHashString)
        .digest('hex');
    res.render('pay', { title: 'Оплата', mrh_login, out_sum, inv_id, inv_desc, md5Hash});
});

router.get('/:orderid', function(req, res, next) {


    Order.findOne({ _id: req.params.orderid }, function(err, order) {
        if (!order) {
            req.flash('error', `Некорректный счет: ${req.params.orderid}`);
            return res.redirect('/lk');
        }
        let mrh_login = config.robokassa.mrh_login;
        let mrh_pass1 = config.robokassa.mrh_pass1;
        let inv_id = +order.number;
        let inv_desc = "Access to D-opros.com";
        let out_sum = prices.register;
        let toHashString = `${mrh_login}:${out_sum}:${inv_id}:${mrh_pass1}`;
        let md5Hash = crypto.createHash('md5')
            .update(toHashString)
            .digest('hex');

        res.render('pay', { title: 'Оплата', mrh_login, out_sum, inv_id, inv_desc, md5Hash, 'orderId' : req.params.orderid});
    });
});

router.post('/result', function (req, res, next) {
    let OutSum = req.body.OutSum || req.query.OutSum || '';
    console.log(OutSum);
    let InvId = req.body.InvId || req.query.InvId || '';
    let mrh_pass2 = config.robokassa.mrh_pass2;
    let SignatureValue = req.body.SignatureValue || req.query.SignatureValue || '';
    let toHashString = `${OutSum}:${InvId}:${mrh_pass2}`;
    let md5Hash = crypto.createHash('md5')
        .update(toHashString)
        .digest('hex');
    console.log(md5Hash.toUpperCase(), SignatureValue);
    if (md5Hash.toUpperCase() !== SignatureValue){
        res.status(403).end();
    } else {
        async.waterfall([
            function (callback) {
                Order.findOneAndUpdate({number : InvId}, {isPaid : true, datePay : Date.now() }, callback);
            },
            function (order, callback) {
                console.log(order);
                User.findByIdAndUpdate(order.userId, {status : 1}, callback)
            },
            function (user, callback) {
                User.findById(user.partnerId, function (err, partner1) {
                    if (err) {return callback(err)}
                    callback(null, user.id, user.partnerId, partner1.partnerId)
                })
            }
        ], function (err, user, partner1, partner2) {
            if (err) res.status(500).end();
            User.findByIdAndUpdate(user, {status: 1});
            if (partner1) {
                let note1 = new PollfromRegister({
                    fromUser: user,
                    toUser: partner1,
                    summ: +config.prices.partner1thLevel
                });
                note1.save();
            }
            if (partner2) {
                let note2 = new PollfromRegister({
                    fromUser: user,
                    toUser: partner2,
                    summ: +config.prices.partner2thLevel
                });
                note2.save();
            }
            res.status(200).send('OK'+InvId);
        })
    }
});

module.exports = router;
