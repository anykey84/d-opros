const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const async = require('async');
const _ = require('lodash');


/* GET home page. */
router.get('/', function (req, res, next) {
    if (req.user){
        res.render('lk');
    } else{
        res.render('login', {title: 'Вход в личный кабинет'});
    }
});

router.post('/', function (req, res, next) {
    const username = req.body.username;
    const password = req.body.password;
    async.waterfall([
        function (callback) {
            User.findOne({email_confirmed: username}, callback);
        },
        function (user, callback) {
            if (user) return callback(null, user);
            User.findOne({email: username}, callback);
        },
        function (user, callback) {
            if (user) {
                if (user.checkPassword(password)) {
                    callback(null, user);
                } else {
                    callback(null, null, ['Неверный пользователь или пароль']);
                }
            } else {
                callback(null, null);
            }
        }
    ], function (err, user, result) {
        if (err) return next(err);
        if (!user) {
            req.flash('error', 'Неверный пользователь или пароль');
            res.redirect('/login');
        } else {
            req.session.user = user._id;
            if (_.isArray(result)) {
                req.flash('error', _.toString(result).replace(',', '<br>'));
                res.redirect('/login');
            } else {
                res.redirect('/lk');
            }
        }
    });
});

module.exports = router;
