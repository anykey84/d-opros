const express = require('express');
const router = express.Router();
const moment = require('moment');
const  async = require('async');
const User = require('../models/user').User;
const PollFromRegister = require('../models/pollFromRegister').PollfromRegister;
const Order = require('../models/order').Order;
const _ = require('lodash');
const mailerTransporter = require('../etc/mailerTransporter');
const config = require('../config/config.json');


/* GET home page. */
router.get('/', function (req, res, next) {
    let expires = false;
    let user = req.user;
    if (moment.unix(user.confirmPhoneTokenExpires / 1000).isBefore(moment())) expires = true;

    async.waterfall([
        function (callback) {
            User.find({partner_id: user.id}, callback)
        },
        function (refs, callback) {
            PollFromRegister.find({toUser : user.id}, function (err, polls) {
               if (err) callback(err);
               callback(null, refs, polls)
            })
        },
        function (refs, polls, callback) {
            Order.findOne({userId : user.id, isPaid : true}, function (err, order) {
                if (err) callback(err);
                callback(null, refs, polls, order);
            })
        }
    ], function (err, refs, polls, order) {
        if (err) next(err);
        let verified = refs.filter(function (ref) {
            return ref.status >= 1;
        });
        let completed = polls.filter(function (poll) {
            return poll.isCompleted === true;
        });
        let sum = _.sumBy(completed, 'summ');
        res.render('lk', {
            expires, host: req.headers.host,
            user,
            refs : {
                registered : refs, verified
            },
            polls : {
                total : polls,
                completed,
                incompleted: polls.length - completed.length
            },
            sum,
            order,
        });
    });
});

router.get('/payout', function (req, res, next) {
    let user = req.user;
    PollFromRegister.find({toUser : user.id, isCompleted : true}, function (err, polls) {
        if (err) callback(err);
        let sum = _.sumBy(polls, 'summ');
        if (sum < 300) {
            req.flash('error', 'Недостаточно средств для вывода');
            res.redirect('/lk');
        }
        res.render('pay-out', {user, sum})
    })
});

router.post('/payout/:ps', function (req, res, next) {
    let ps = req.params.ps;
    let number = req.body.number;
    let sum = req.body.sum;
    console.log(ps, number, sum);
    const mailOptions = {
        to: config.adminEmail ,
        from: config.serverEmail,
        subject: '[d-opros] new payout',
        html: `<p>Заявка на выплату</p>
                            <p>${ps}</p>
                            <p>${number}</p>
                            <p>${sum}</p> `
    };
    mailerTransporter.sendMail(mailOptions, (err => {
        if (err) {
            req.flash('error', 'Ошибка отправки заявки. Попробуйте еще раз');
            res.redirect('/lk/payout');
        } else {
            req.flash('success', 'Заявка на выплату отправлена и будет выполнена в течение 24 часов');
            res.redirect('/lk');
        }
    }));
});

module.exports = router;
