const express = require('express');
const router = express.Router();
const async = require('async');
const User = require('../models/user').User;
const https = require('https');
const moment = require('moment');


router.get('/', function(req, res) {
    res.redirect('/');
});

router.post('/', function(req, res, next) {
    if (req.body.code){
        async.waterfall([
            function(done) {
                User.findOne({ confirmPhoneToken: req.body.code, phone: req.body.phone}, function(err, user) {
                    if (!user) {
                        req.flash('error', 'Неверный код подтверждения');
                        return res.redirect('back');
                    }

                    user.confirmed_phone = user.phone;
                    user.confirmPhoneToken = undefined;
                    user.confirmPhoneTokenExpires = undefined;

                    user.save(function(err) {
                        req.flash('success', 'Телефон успешно подтвержден');
                        return done(err, user);
                    });
                })
            }
        ], function(err) {
            if (err) return next(err);
            res.redirect('/lk');
        });
    } else {
        async.waterfall([
            function(done) {
                User.findOne({ phone: req.body.phone }, function(err, user) {
                    if (!user) {
                        req.flash('error', 'Данный телефон не принадлежит ни одной учетной записи');
                        return res.redirect('/lk');
                    }


                    if (!user.confirmPhoneToken || !user.confirmPhoneTokenExpires){
                        if (!user.confirmPhoneToken) user.confirmPhoneToken = (Math.random() * 10000).toFixed();
                        if (!user.confirmPhoneTokenExpires) user.confirmPhoneTokenExpires = Date.now() + 300000; // 5 minutes
                        user.save(function(err) {
                            done(err, user.confirmPhoneToken, user);
                        });
                    } else {
                        done(null, user.confirmPhoneToken, user);
                    }

                });
            },
            function(token, user, done) {

                if (moment.unix(user.confirmPhoneTokenExpires / 1000).isBefore(moment())){
                    let smsText = 'D-OPROS code: ' + token;
                    console.log(user.phone);
                    https.get('https://gate.smsaero.ru/send/?user=kalimulin-ru@yandex.ru&password=EdAh8z6V1jsqfjmaiNAKitP5kaUH&to='+ user.phone +'&text='+ smsText +'&from=news', (res) => {
                        const { statusCode } = res;
                        const contentType = res.headers['content-type'];

                        let error;
                        if (statusCode !== 200) {
                            error = new Error(`Request Failed.\n` +
                                `Status Code: ${statusCode}`);}
                        if (error) {
                            console.error(error.message);
                            // consume response data to free up memory
                            res.flash('error', 'Ошибка отправки СМС, попробуйте еще раз');
                            return done(error, 'done');
                        }

                        req.flash('info', 'CМС с кодом было отправлено на телефон ' + user.phone);
                        done(null, 'done');

                    }).on('error', (e) => {
                        console.error(`Got error: ${e.message}`);
                        done(e, 'done');
                    });
                } else {
                    req.flash('info', 'Код подтверждения можно запрашивать не раньше чем через 5 минут после предыдущего');
                    done(null, 'done');
                }


            }
        ], function(err) {
            if (err) return next(err);
            res.redirect('/lk');
        });
    }

});

module.exports = router;
