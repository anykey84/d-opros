const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const async = require('async');
const _ = require('lodash');


/* GET home page. */
router.get('/', function (req, res, next) {
    req.session.destroy();
    res.redirect(('/'));
});

module.exports = router;
