const express = require('express');
const router = express.Router();
const async = require('async');
const mailerTransporter = require('../etc/mailerTransporter');
const crypto = require('crypto');
const User = require('../models/user').User;
const config = require('../config/config.json');


router.get('/', function(req, res) {
    res.render('forgot', {
        user: req.user
    });
});

router.post('/', function(req, res, next) {
    async.waterfall([
        function(done) {
            crypto.randomBytes(20, function(err, buf) {
                const token = buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done) {
            User.findOne({ confirmed_email: req.body.email }, function(err, user) {
                if (!user) {
                    req.flash('error', 'Данный email не принадлежит ни одной учетной записи');
                    return res.redirect('/forgot');
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function(err) {
                    done(err, token, user);
                });
            });
        },
        function(token, user, done) {
            const mailOptions = {
                to: user.email,
                from: config.serverEmail,
                subject: 'Password Reset',
                html: '<p>Вы получили это письмо потому, что Вы (или кто-то другой) запросили сброс пароля.</p>' +
                '<p>Для продолжения кликните по ссылке или скопируйте ее в адресную строку браузера:</p>' +
                '<a href="http://' + req.headers.host + '/reset/' + token + '">' +
                'http://' + req.headers.host + '/reset/' + token  +
                '</a>'  +
                '<p>Если Вы не запрашивали смену пароля, игнорируйте это письмо</p>'
            };
            mailerTransporter.sendMail(mailOptions, function(err) {
                req.flash('info', 'Письмо с дальнейшими инструкциями было отправлено по адресу ' + user.email);
                done(err, 'done');
            });
        }
    ], function(err) {
        if (err) return next(err);
        res.redirect('/forgot');
    });
});

module.exports = router;
