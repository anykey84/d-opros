const mongoose = require('./etc/mongoose');
const async = require('async');

async.series([
    open,
    requireModels,
    createQuestions
], function (err) {
    console.log(arguments);
    mongoose.disconnect();
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function requireModels(callback) {
    require('./models/question').Question;
    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback)
}

function createQuestions(callback) {

    const questions = [
        {
            order : 1,
            type: 'radio',
            yourOption : true,
            text : "Цвет, который вызывает доверие",
            answers : [
                {
                    1 : "Синий",
                    2 : "Желтый",
                    3 : "Розовый",
                    4 : "Зеленый",
                }
            ]
        },
        {
            order : 2,
            type: 'radio',
            yourOption : true,
            text : "Цвет, который вызывает неприятное ощущение",
            answers : [
                {
                    1 : "Черный",
                    2 : "Красный",
                    3 : "Синий",
                    4 : "Оранжевый",
                }
            ]
        },
        {
            order : 3,
            type: 'radio',
            yourOption : true,
            text : "Вы в данное время",
            answers : [
                {
                    1 : "Работаете",
                    2 : "Учитесь",
                    3 : "Временно безработный",
                    4 : "Домохозяйка",
                    5 : "Декретный отпуск"
                }
            ]
        },
        {
            order : 4,
            type: 'radio',
            yourOption : false,
            text : "Сколько у Вас детей",
            answers : [
                {
                    1 : "1",
                    2 : "2",
                    3 : "3",
                    4 : "Более 3",
                    5 : "Нет детей"
                }
            ]
        },
        {
            order : 5,
            type: 'radio',
            yourOption : true,
            text : "Какой у Вас телефонный аппарат",
            answers : [
                {
                    1 : "Samsung",
                    2 : "iPhone",
                    3 : "HTC",
                    4 : "простой с кнопками"
                }
            ]
        },
        {
            order : 6,
            type: 'radio',
            yourOption : true,
            text : "Каким оператором сотовой связи Вы пользуетесь",
            answers : [
                {
                    1 : "Мегафон",
                    2 : "Билайн",
                    3 : "МТС",
                    4 : "Теле-2",
                    5 : "Yota"
                }
            ]
        },
        {
            order : 7,
            type: 'radio',
            yourOption : true,
            text : "Вы кладете деньги на баланс телефона",
            answers : [
                {
                    1 : "Терминал в магазине",
                    2 : "Карта своего банка",
                    3 : "Сотовые магазины",
                    4 : "Не кладу вообще",
                }
            ]
        },
        {
            order : 8,
            type: 'radio',
            yourOption : true,
            text : "Вы часто пользуетесь",
            answers : [
                {
                    1 : "Viber",
                    2 : "Whatsapp",
                    3 : "Skype",
                    4 : "СМС-сообщения",
                }
            ]
        },
        {
            order : 9,
            type: 'radio',
            yourOption : true,
            text : "Ваша любимая социальная сеть",
            answers : [
                {
                    1 : "VK.com (ВКонтакте)",
                    2 : "OK.ru (Одноклассники)",
                    3 : "Facebook",
                    4 : "Instagram",
                }
            ]
        },
        {
            order : 10,
            type: 'radio',
            yourOption : true,
            text : "Вам интересны новости",
            answers : [
                {
                    1 : "О политике/о экономике",
                    2 : "О путешествиях",
                    3 : "О кино",
                    4 : "О мероприятиях в Вашем городе",
                }
            ]
        },
        {
            order : 11,
            type: 'radio',
            yourOption : true,
            text : "Какой рекламе Вы доверяете",
            answers : [
                {
                    1 : "ТВ",
                    2 : "Радио",
                    3 : "Газета",
                    4 : "Интернет",
                    5 : "Смс-сообщения",
                    6 : "Рекомендация друга",
                    7 : "Не верю рекламе",
                }
            ]
        },
        {
            order : 12,
            type: 'radio',
            yourOption : false,
            text : "Каким видом транспорта Вы пользуетесь",
            answers : [
                {
                    1 : "Общественный транспорт",
                    2 : "Такси",
                    3 : "Свой автомобиль",
                    4 : "Хожу пешком",
                    5 : "Свой вариант"
                }
            ]
        },
        {
            order : 13,
            type: 'radio',
            yourOption : true,
            text : "Чей Вы клиент",
            answers : [
                {
                    1 : "Банк",
                    2 : "Микрозаймы",
                    3 : "Член кооператива",
                    4 : "Дружеский долг",
                }
            ]
        },
        {
            order : 14,
            type: 'radio',
            yourOption : true,
            text : "Как часто Вы берете деньги в долг?",
            answers : [
                {
                    1 : "Каждый день",
                    2 : "Раз в неделю",
                    3 : "Раз в месяц",
                    4 : "Никогда",
                }
            ]
        },
        {
            order : 15,
            type: 'radio',
            yourOption : true,
            text : "Выберите магазин бытовой техники, которому доверяете",
            answers : [
                {
                    1 : "Эльдорадо",
                    2 : "М-видео",
                    3 : "Гефест",
                    4 : "Покупаю в интернете",
                    5 : "Нет детей"
                }
            ]
        },
        {
            order : 16,
            type: 'radio',
            yourOption : false,
            text : "Какое у Вас любимое время года",
            answers : [
                {
                    1 : "Зима",
                    2 : "Весна",
                    3 : "Лето",
                    4 : "Осень",
                }
            ]
        },
        {
            order : 17,
            type: 'radio',
            yourOption : true,
            text : "Ваша любимая страна",
            answers : [
                {
                    1 : "Россия",
                    2 : "Испания",
                    3 : "Тайланд",
                    4 : "Турция",
                    5 : "Египет"
                }
            ]
        },
        {
            order : 18,
            type: 'radio',
            yourOption : true,
            text : " Какое блюдо Ваше любимое",
            answers : [
                {
                    1 : "Борщ",
                    2 : "Пицца",
                    3 : "Рыбные",
                    4 : "Шаверма",
                }
            ]
        },
        {
            order : 19,
            type: 'radio',
            yourOption : true,
            text : "Вы являетесь",
            answers : [
                {
                    1 : "Мясоед",
                    2 : "Вегетарианец",
                    3 : "Только сырые овощи",
                    4 : "Питаюсь солнечной энергией",
                }
            ]
        },
        {
            order : 20,
            type: 'radio',
            yourOption : true,
            text : "Любимый жанр кино",
            answers : [
                {
                    1 : "Классика",
                    2 : "Мультфильм",
                    3 : "Ужасы",
                    4 : "Мелодрама",
                    5 : "Комедия",
                    6 : "Не люблю кино"
                }
            ]
        },
        {
            order : 21,
            type: 'radio',
            yourOption : false,
            text : "Сколько у Вас детей",
            answers : [
                {
                    1 : "1",
                    2 : "2",
                    3 : "3",
                    4 : "Более 3",
                    5 : "Нет детей"
                }
            ]
        },
    ];
    async.each(questions, function (questionData, callback) {
        const question = new mongoose.models.Question(questionData);
        question.save(callback);
    }, callback);
}
