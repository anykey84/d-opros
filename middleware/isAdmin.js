const Log = require('../models/log').Log;

module.exports = function (req, res, next) {
    if (req.user && req.user.status > 0) {
        const log = new Log({userid : req.user.id, message : req.baseUrl + req.path});
        log.save();
    }
    if (req.user && req.user.status > 90){
        return next();
    }
    res.redirect('/login')
};