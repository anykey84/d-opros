module.exports = function (res, req, next) {
    if (!req.session.user){
        return next({status: 401, message: 'Вы не авторизованы'});
    }
    next();
};