const mongoose = require('./etc/mongoose');
const async = require('async');

async.series([
    open,
    requireModels,
    createPFRS
], function (err) {
    console.log(arguments);
    mongoose.disconnect();
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function requireModels(callback) {
    require('./models/pollFromRegister').PollfromRegister;
    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback)
}

function createPFRS(callback) {

    const pfrs = [
        {
            "fromUser" : "5982ad8b6e8af03aecd64de3",
            "toUser" : "59802842f47c382deca5cba7",
            "summ" : 150,
            "isCompleted" : false
        },
        {
            "fromUser" : "5982ad8b6e8af03aecd64de3",
            "toUser" : "59802842f47c382deca5cba7",
            "summ" : 150,
            "isCompleted" : false
        },
        {
            "fromUser" : "5982ad8b6e8af03aecd64de3",
            "toUser" : "59802842f47c382deca5cba7",
            "summ" : 150,
            "isCompleted" : false
        },
        {
            "fromUser" : "5982ad8b6e8af03aecd64de3",
            "toUser" : "59802842f47c382deca5cba7",
            "summ" : 150,
            "isCompleted" : false
        },
    ];
    async.each(pfrs, function (pfrsData, callback) {
        const pfr = new mongoose.models.PollfromRegister(pfrsData);
        pfr.save(callback);
    }, callback);
}
